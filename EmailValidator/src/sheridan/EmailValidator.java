package sheridan;

import java.util.logging.Logger;

/*
 * This is The email validator class with all the required functions
 * Author: Divjot Chawla
 * Version: 2.0
 */

public class EmailValidator {

	
  static Logger logger = Logger.getLogger(EmailValidator.class.getName()); 
	 
	public static void main(String args[]) {
		String email = "chawladivjot10@gmail.com";
	}
	
	public static boolean isValidAccountName(String accountName) {
		Boolean isValid = false;
		
		if(accountName.length() >= 3) {
			
			int atLeastThreeLowerCase = 0;
			
			for(int i = 0; i < accountName.length(); i++)
			{
				if(Character.isLowerCase(accountName.charAt(i)))
				{
					atLeastThreeLowerCase++;
				}
			}
			
			if(atLeastThreeLowerCase >= 3) {
				
				if(Character.isDigit(accountName.charAt(0))) {
					throw new NumberFormatException("Accountname can not start with a number.");
				} 
				
				else {
					isValid = true;
				}
			} 
			
			else {
				throw new NumberFormatException("Accountname must contain atleast 3 lowercase characters.");
			}
		} 
		
		else 
		{
			throw new NumberFormatException("Accountname should be alteast 3 or more character in length.");
		}
		
		return isValid;
	}
	
	public static boolean isValidDomainName(String domainName) {
		Boolean isValid = false;
		
		if(domainName.length() >= 3) {
			
			int atLeastThreeLowerCase = 0;
			int atLeastThreeDigit = 0;
			
			for(int i = 0; i < domainName.length(); i++)
			{
				if((Character.isLowerCase(domainName.charAt(i))))
				{
					atLeastThreeLowerCase++;
				}
				
				else if (Character.isDigit(domainName.charAt(i))) {
					atLeastThreeDigit++;
				}
			}
			
			if((atLeastThreeLowerCase >= 3) || (atLeastThreeDigit >= 3)) {			
				isValid = true;			
			}
			
			else {
				throw new NumberFormatException("Domainname should be atleast 3 lower case characters.");
			}
			
		}
		
		else {
			throw new NumberFormatException("Domainname should be atleast 3 or more characters!");
		}
		return isValid;
	}
	
	public static boolean isValidExtension(String extensionName) {
		Boolean isValid = false;
		
		if(extensionName.length() >= 2) {
			Boolean isNumber = false;
			
			for(int i = 0; i < extensionName.length(); i++) {
				if(Character.isDigit(extensionName.charAt(i))) {
					isNumber = true;
				}
			}
			
			if(isNumber) {
				throw new NumberFormatException("Extensionname should not have a number!");
			}
			
			else {
				isValid = true;
			}			
		}
		
		else {
			throw new NumberFormatException("Extensionname should be atleast 2 characters!");
		}
		
		return isValid;
	}
	
	public static boolean isValidEmail(String email) {
		Boolean isValid = false;
		
		int totalAmountOfChracters = email.trim().length();				
		int atCharacterFoundIndex = email.trim().indexOf('@');
		int periodCharacterFoundIndex = email.trim().indexOf('.');		
		String accountName = "";
		String domainName = "";
		String extensionName = "";
		try {
			accountName = String.valueOf(email.trim().substring(0, atCharacterFoundIndex));
			domainName = String.valueOf(email.trim().substring(atCharacterFoundIndex+1, periodCharacterFoundIndex));
			extensionName =  String.valueOf(email.trim().substring(periodCharacterFoundIndex+1, totalAmountOfChracters));
		} catch (Exception e) {
			throw new NumberFormatException("One part of the email can not be null!");
		}		
		
		if((email.contains("@")) && (email.indexOf('@')) == (email.lastIndexOf('@')) &&
			(email.contains(".") && (email.indexOf('.')) == (email.lastIndexOf('.')))) {	
			
			if(isValidAccountName(accountName) && isValidDomainName(domainName) && isValidExtension(extensionName))
				isValid = true;

		} else {
			throw new NumberFormatException("Wrong! should follow format of '<account>@<domain>.<extension>!");
		}
		return isValid;
	}
}
