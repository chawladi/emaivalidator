package test;

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.EmailValidator;

/*
 * This is The test class with all the required functions
 * Author: Divjot Chawla
 */

public class EmailValidatorTest {

	// The four test methods are for formatting 
  @Test
  public void testIsValidEmailFormatRegular() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladivjot10@gmail.ca");
    assertTrue("Invalid Email Format!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidEmailFormatException() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladivjot10@gmailca");
    assertFalse("Invalid Email Format!", isValidCase);
		}
		
  @Test
  public void testIsValidEmailFormatBoundaryIn() {
    boolean isValidCase = EmailValidator.isValidEmail("divjot@gmail.com");
    assertTrue("Invalid Email Format!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidEmailFormatBoundaryOut() {
    boolean isValidCase = EmailValidator.isValidEmail("divjot@gmail.com.");
    assertTrue("Invalid Email Format!", isValidCase);
		}
		
	// The four test methods are for domain name
		
  @Test
  public void testIsValidDomainNameRegular() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@outlook.ca");
    assertTrue("Invalid Domain Name!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidDomainNameException() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@.ca");
    assertFalse("Invalid Domain Name!", isValidCase);
		}
		
  @Test
  public void testIsValidDomainNameBoundaryIn() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@out.ca");
    assertTrue("Invalid Domain Name!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidDomainNameBoundaryOut() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@o.ca");
    assertFalse("Invalid Domain Name!", isValidCase);
		}
		
		
		// The four test methods are for extension
		
  @Test
  public void testIsValidExtensionNameRegular() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@outlook.ca");
    assertTrue("Invalid Extension Name!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidExtensionNameException() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@outlook.");
    assertFalse("Invalid Extension Name!", isValidCase);
		}
		
		
  @Test
  public void testIsValidExtensionNameBoundaryIn() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@outlook.com");
    assertTrue("Invalid Extension Name!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidExtensionNameBoundaryOut() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@outlook.c");
    assertFalse("Invalid Extension Name!", isValidCase);
		}
		
		// The four test methods are for only having one @
		
  @Test
  public void testIsValidOnlyOneAtSymbolRegular() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladivjot10@gmail.ca");
    assertTrue("Invalid Email Format!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidOnlyOneAtSymbolException() {
    boolean isValidCase = EmailValidator.isValidEmail("chawla@10@gmail.ca");
    assertFalse("Invalid Email Format!", isValidCase);
		}
		
  @Test
  public void testIsValidOnlyOneAtSymbolBoundaryIn() {
    boolean isValidCase = EmailValidator.isValidEmail("divjot@gmail.com");
    assertTrue("Invalid Email Format!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidOnlyOneAtSymbolBoundaryOut() {
    boolean isValidCase = EmailValidator.isValidEmail("divjot@@gmail.com");
    assertTrue("Invalid Email Format!", isValidCase);
		}

		// The four test methods are for account name
		
  @Test
  public void testIsValidAccountNameRegular() {
    boolean isValidCase = EmailValidator.isValidEmail("chawladi10@outlook.ca");
    assertTrue("Invalid Account Name!", isValidCase);
		}
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidAccountNameException() {
    boolean isValidCase = EmailValidator.isValidEmail("@outlook.ca");
    assertFalse("Invalid Account Name!", isValidCase);
		}
		
  @Test
  public void testIsValidAccountNameBoundaryIn() {
    boolean isValidCase = EmailValidator.isValidEmail("chawlladi@outlook.ca");
    assertTrue("Invalid Account Name!", isValidCase);
		}
		
		
  @Test(expected=NumberFormatException.class)
  public void testIsValidAccountNameBoundaryOut() {
    boolean isValidCase = EmailValidator.isValidEmail("DIV@outlook.ca");
    assertFalse("Invalid Account Name!", isValidCase);
		}

	
		
		
		
	}
